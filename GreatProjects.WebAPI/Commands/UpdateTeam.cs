﻿using AutoMapper;
using GreatProjects.DAL.DataAccess;
using GreatProjects.WebAPI.DTOs;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace GreatProjects.WebAPI.Commands
{
    public static class UpdateTeam
    {
        public record Command(TeamDTO Team) : IRequest<Response>;

        public class Handler : IRequestHandler<Command, Response>
        {
            private readonly IMapper mapper;
            private readonly ProjectContext projectContext;

            public Handler(IMapper mapper, ProjectContext projectContext)
            {
                this.mapper = mapper;
                this.projectContext = projectContext;
            }

            public async Task<Response> Handle(Command request, CancellationToken cancellationToken)
            {
                var team = await projectContext.Teams
                    .FindAsync(new object[] { request.Team.Id }, cancellationToken);
                
                mapper.Map(request.Team, team);
                await projectContext.SaveChangesAsync(cancellationToken);

                return new Response { Id = team.Id };
            }
        }

        public record Response
        {
            public int Id { get; init; }
        }
    }
}
