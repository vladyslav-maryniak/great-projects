﻿using AutoMapper;
using GreatProjects.DAL.DataAccess;
using GreatProjects.SharedLibrary.Models;
using GreatProjects.WebAPI.DTOs;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace GreatProjects.WebAPI.Commands
{
    public static class AddTeam
    {
        public record Command(TeamDTO Team) : IRequest<Response>;

        public class Handler : IRequestHandler<Command, Response>
        {
            private readonly IMapper mapper;
            private readonly ProjectContext projectContext;

            public Handler(IMapper mapper, ProjectContext projectContext)
            {
                this.mapper = mapper;
                this.projectContext = projectContext;
            }

            public async Task<Response> Handle(Command request, CancellationToken cancellationToken)
            {
                var result = await projectContext.Teams
                    .AddAsync(mapper.Map<Team>(request.Team), cancellationToken);
                
                await projectContext.SaveChangesAsync(cancellationToken);

                return new Response { Id = result.Entity.Id };
            }
        }

        public record Response
        {
            public int Id { get; init; }
        }
    }
}
