﻿using AutoMapper;
using GreatProjects.DAL.DataAccess;
using GreatProjects.WebAPI.DTOs;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace GreatProjects.WebAPI.Commands
{
    public static class AddTask
    {
        public record Command(TaskDTO Task) : IRequest<Response>;

        public class Handler : IRequestHandler<Command, Response>
        {
            private readonly IMapper mapper;
            private readonly ProjectContext projectContext;

            public Handler(IMapper mapper, ProjectContext projectContext)
            {
                this.mapper = mapper;
                this.projectContext = projectContext;
            }

            public async Task<Response> Handle(Command request, CancellationToken cancellationToken)
            {
                var result = await projectContext.Tasks
                    .AddAsync(mapper.Map<SharedLibrary.Models.Task>(request.Task), cancellationToken);
                
                await projectContext.SaveChangesAsync(cancellationToken);

                return new Response { Id = result.Entity.Id };
            }
        }

        public record Response
        {
            public int Id { get; init; }
        }
    }
}
