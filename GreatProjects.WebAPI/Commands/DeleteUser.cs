﻿using AutoMapper;
using GreatProjects.DAL.DataAccess;
using GreatProjects.SharedLibrary.Models;
using GreatProjects.WebAPI.DTOs;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace GreatProjects.WebAPI.Commands
{
    public static class DeleteUser
    {
        public record Command(int Id) : IRequest<Response>;

        public class Handler : IRequestHandler<Command, Response>
        {
            private readonly IMapper mapper;
            private readonly ProjectContext projectContext;

            public Handler(IMapper mapper, ProjectContext projectContext)
            {
                this.mapper = mapper;
                this.projectContext = projectContext;
            }

            public async Task<Response> Handle(Command request, CancellationToken cancellationToken)
            {
                var user = await projectContext
                    .FindAsync<User>(new object[] { request.Id }, cancellationToken);
                
                projectContext.Users.Remove(user);
                await projectContext.SaveChangesAsync(cancellationToken);

                return new Response { User = mapper.Map<UserDTO>(user) };
            }
        }

        public record Response
        {
            public UserDTO User { get; init; }
        }
    }
}
