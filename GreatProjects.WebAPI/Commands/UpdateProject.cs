﻿using AutoMapper;
using GreatProjects.DAL.DataAccess;
using GreatProjects.WebAPI.DTOs;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace GreatProjects.WebAPI.Commands
{
    public static class UpdateProject
    {
        public record Command(ProjectDTO Project) : IRequest<Response>;

        public class Handler : IRequestHandler<Command, Response>
        {
            private readonly IMapper mapper;
            private readonly ProjectContext projectContext;

            public Handler(IMapper mapper, ProjectContext projectContext)
            {
                this.mapper = mapper;
                this.projectContext = projectContext;
            }

            public async Task<Response> Handle(Command request, CancellationToken cancellationToken)
            {
                var project = await projectContext.Projects
                    .FindAsync(new object[] { request.Project.Id }, cancellationToken);

                mapper.Map(request.Project, project);
                await projectContext.SaveChangesAsync(cancellationToken);

                return new Response { Id = project.Id };
            }
        }

        public record Response
        {
            public int Id { get; init; }
        }
    }
}
