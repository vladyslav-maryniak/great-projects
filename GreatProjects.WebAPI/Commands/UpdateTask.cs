﻿using AutoMapper;
using GreatProjects.DAL.DataAccess;
using GreatProjects.WebAPI.DTOs;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace GreatProjects.WebAPI.Commands
{
    public static class UpdateTask
    {
        public record Command(TaskDTO Task) : IRequest<Response>;

        public class Handler : IRequestHandler<Command, Response>
        {
            private readonly IMapper mapper;
            private readonly ProjectContext projectContext;

            public Handler(IMapper mapper, ProjectContext projectContext)
            {
                this.mapper = mapper;
                this.projectContext = projectContext;
            }

            public async Task<Response> Handle(Command request, CancellationToken cancellationToken)
            {
                var task = await projectContext.Tasks
                    .FindAsync(new object[] { request.Task.Id }, cancellationToken);
                
                mapper.Map(request.Task, task);
                await projectContext.SaveChangesAsync(cancellationToken);

                return new Response { Id = task.Id };
            }
        }

        public record Response
        {
            public int Id { get; init; }
        }
    }
}
