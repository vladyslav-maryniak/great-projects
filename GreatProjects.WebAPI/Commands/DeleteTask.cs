﻿using AutoMapper;
using GreatProjects.DAL.DataAccess;
using GreatProjects.WebAPI.DTOs;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace GreatProjects.WebAPI.Commands
{
    public static class DeleteTask
    {
        public record Command(int Id) : IRequest<Response>;

        public class Handler : IRequestHandler<Command, Response>
        {
            private readonly IMapper mapper;
            private readonly ProjectContext projectContext;

            public Handler(IMapper mapper, ProjectContext projectContext)
            {
                this.mapper = mapper;
                this.projectContext = projectContext;
            }

            public async Task<Response> Handle(Command request, CancellationToken cancellationToken)
            {
                var task = await projectContext
                    .FindAsync<SharedLibrary.Models.Task>(new object[] { request.Id }, cancellationToken);
                
                projectContext.Tasks.Remove(task);
                await projectContext.SaveChangesAsync(cancellationToken);

                return new Response { Task = mapper.Map<TaskDTO>(task) };
            }
        }

        public record Response
        {
            public TaskDTO Task { get; init; }
        }
    }
}
