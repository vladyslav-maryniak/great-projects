﻿using AutoMapper;
using GreatProjects.DAL.DataAccess;
using GreatProjects.WebAPI.DTOs;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace GreatProjects.WebAPI.Commands
{
    public static class UpdateUser
    {
        public record Command(UserDTO User) : IRequest<Response>;

        public class Handler : IRequestHandler<Command, Response>
        {
            private readonly IMapper mapper;
            private readonly ProjectContext projectContext;

            public Handler(IMapper mapper, ProjectContext projectContext)
            {
                this.mapper = mapper;
                this.projectContext = projectContext;
            }

            public async Task<Response> Handle(Command request, CancellationToken cancellationToken)
            {
                var user = await projectContext.Users
                    .FindAsync(new object[] { request.User.Id }, cancellationToken);
                
                mapper.Map(request.User, user);
                await projectContext.SaveChangesAsync(cancellationToken);

                return new Response { Id = user.Id };
            }
        }

        public record Response
        {
            public int Id { get; init; }
        }
    }
}
