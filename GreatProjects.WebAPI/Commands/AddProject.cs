﻿using AutoMapper;
using GreatProjects.DAL.DataAccess;
using GreatProjects.SharedLibrary.Models;
using GreatProjects.WebAPI.DTOs;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace GreatProjects.WebAPI.Commands
{
    public static class AddProject
    {
        public record Command(ProjectDTO Project) : IRequest<Response>;

        public class Handler : IRequestHandler<Command, Response>
        {
            private readonly IMapper mapper;
            private readonly ProjectContext projectContext;

            public Handler(IMapper mapper, ProjectContext projectContext)
            {
                this.mapper = mapper;
                this.projectContext = projectContext;
            }

            public async Task<Response> Handle(Command request, CancellationToken cancellationToken)
            {
                var result = await projectContext.Projects
                    .AddAsync(mapper.Map<Project>(request.Project), cancellationToken);
                
                await projectContext.SaveChangesAsync(cancellationToken);

                return new Response { Id = result.Entity.Id };
            }
        }

        public record Response
        {
            public int Id { get; init; }
        }
    }
}
