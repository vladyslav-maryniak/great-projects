﻿using AutoMapper;
using GreatProjects.DAL.DataAccess;
using GreatProjects.SharedLibrary.Models;
using GreatProjects.WebAPI.DTOs;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace GreatProjects.WebAPI.Commands
{
    public static class DeleteProject
    {
        public record Command(int Id) : IRequest<Response>;

        public class Handler : IRequestHandler<Command, Response>
        {
            private readonly IMapper mapper;
            private readonly ProjectContext projectContext;

            public Handler(IMapper mapper, ProjectContext projectContext)
            {
                this.mapper = mapper;
                this.projectContext = projectContext;
            }

            public async Task<Response> Handle(Command request, CancellationToken cancellationToken)
            {
                var project = await projectContext
                    .FindAsync<Project>(new object[] { request.Id }, cancellationToken);

                projectContext.Projects.Remove(project);
                await projectContext.SaveChangesAsync(cancellationToken);

                return new Response { Project = mapper.Map<ProjectDTO>(project) };
            }
        }

        public record Response
        {
            public ProjectDTO Project { get; init; }
        }
    }
}
