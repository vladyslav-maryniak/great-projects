﻿using AutoMapper;
using GreatProjects.DAL.DataAccess;
using GreatProjects.SharedLibrary.Models;
using GreatProjects.WebAPI.DTOs;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace GreatProjects.WebAPI.Commands
{
    public static class DeleteTeam
    {
        public record Command(int Id) : IRequest<Response>;

        public class Handler : IRequestHandler<Command, Response>
        {
            private readonly IMapper mapper;
            private readonly ProjectContext projectContext;

            public Handler(IMapper mapper, ProjectContext projectContext)
            {
                this.mapper = mapper;
                this.projectContext = projectContext;
            }

            public async Task<Response> Handle(Command request, CancellationToken cancellationToken)
            {
                var team = await projectContext
                    .FindAsync<Team>(new object[] { request.Id }, cancellationToken);
                
                projectContext.Teams.Remove(team);
                await projectContext.SaveChangesAsync(cancellationToken);

                return new Response { Team = mapper.Map<TeamDTO>(team) };
            }
        }

        public record Response
        {
            public TeamDTO Team { get; init; }
        }
    }
}
