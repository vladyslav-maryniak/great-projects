﻿using GreatProjects.SharedLibrary.ReadModels;
using GreatProjects.WebAPI.Commands;
using GreatProjects.WebAPI.DTOs;
using GreatProjects.WebAPI.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GreatProjects.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IMediator mediator;

        public ProjectsController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProjectDTO>>> GetProjects()
        {
            var response = await mediator.Send(new GetProjects.Query());
            return response == null ? NotFound() : Ok(response.Projects);
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<ProjectDTO>> GetProjectById(int id)
        {
            var response = await mediator.Send(new GetProjectById.Query(id));
            return response == null ? NotFound() : Ok(response.Project);
        }

        [HttpGet("metrics")]
        public async Task<ActionResult<IAsyncEnumerable<ProjectMetrics>>> GetMetricsOfProjects()
        {
            var response = await mediator.Send(new GetMetricsOfProjects.Query());
            return response == null ? NotFound() : Ok(response);
        }

        [HttpPost("")]
        public async Task<ActionResult<int>> AddProject(ProjectDTO project)
        {
            var response = await mediator.Send(new AddProject.Command(project));
            return response == null ? NotFound() : Ok(response);
        }

        [HttpDelete("{projectId:int}")]
        public async Task<ActionResult<ProjectDTO>> DeleteProject(int projectId)
        {
            var response = await mediator.Send(new DeleteProject.Command(projectId));
            return response == null ? NotFound() : Ok(response.Project);
        }

        [HttpPut("")]
        public async Task<ActionResult<ProjectDTO>> UpdateProject(ProjectDTO project)
        {
            var response = await mediator.Send(new UpdateProject.Command(project));
            return response == null ? NotFound() : Ok(response);
        }
    }
}
