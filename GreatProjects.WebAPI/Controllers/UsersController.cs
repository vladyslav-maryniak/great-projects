﻿using GreatProjects.SharedLibrary.ReadModels;
using GreatProjects.WebAPI.Commands;
using GreatProjects.WebAPI.DTOs;
using GreatProjects.WebAPI.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GreatProjects.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IMediator mediator;
        
        public UsersController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserDTO>>> GetUsers()
        {
            var response = await mediator.Send(new GetUsers.Query());
            return response == null ? NotFound() : Ok(response.Users);
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<UserDTO>> GetUserById(int id)
        {
            var response = await mediator.Send(new GetUserById.Query(id));
            return response == null ? NotFound() : Ok(response.User);
        }

        [HttpGet("{userId:int}/task-count-per-project")]
        public async Task<ActionResult<IAsyncEnumerable<ProjectNumberOfTasks>>> GetTaskCountPerProject(int userId)
        {
            var response = await mediator.Send(new GetNumberOfTasksInUserProject.Query(userId));
            return response == null ? NotFound() : Ok(response);
        }

        [HttpGet("{userId:int}/restricted-tasks")]
        public async Task<ActionResult<IAsyncEnumerable<RestrictedTask>>> GetRestrictedTasks(int userId)
        {
            var response = await mediator.Send(new GetUserTasksWhereNameIsLessThan45.Query(userId));
            return response == null ? NotFound() : Ok(response);
        }

        [HttpGet("{userId:int}/completed-tasks")]
        public async Task<ActionResult<IAsyncEnumerable<CompletedTask>>> GetCompletedTasks(int userId)
        {
            var response = await mediator.Send(new GetCompletedUserTasksIn2021.Query(userId));
            return response == null ? NotFound() : Ok(response);
        }

        [HttpGet("sorted-users-with-sorted-tasks")]
        public async Task<ActionResult<IAsyncEnumerable<CompletedTask>>> GetSortedUsersWithSortedTasks()
        {
            var response = await mediator.Send(new GetSortedUsersWithSortedTasks.Query());
            return response == null ? NotFound() : Ok(response);
        }

        [HttpGet("{userId:int}/metrics")]
        public async Task<ActionResult<UserMetrics>> GetUserMetrics(int userId)
        {
            var response = await mediator.Send(new GetUserMetrics.Query(userId));
            return response == null ? NotFound() : Ok(response);
        }

        [HttpPost("")]
        public async Task<ActionResult<int>> AddUser(UserDTO user)
        {
            var response = await mediator.Send(new AddUser.Command(user));
            return response == null ? NotFound() : Ok(response);
        }

        [HttpDelete("{userId:int}")]
        public async Task<ActionResult<UserDTO>> DeleteUser(int userId)
        {
            var response = await mediator.Send(new DeleteUser.Command(userId));
            return response == null ? NotFound() : Ok(response.User);
        }

        [HttpPut("")]
        public async Task<ActionResult<UserDTO>> UpdateUser(UserDTO user)
        {
            var response = await mediator.Send(new UpdateUser.Command(user));
            return response == null ? NotFound() : Ok(response);
        }
    }
}
