﻿using GreatProjects.SharedLibrary.ReadModels;
using GreatProjects.WebAPI.Commands;
using GreatProjects.WebAPI.DTOs;
using GreatProjects.WebAPI.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GreatProjects.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly IMediator mediator;

        public TeamsController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<TeamDTO>>> GetTeams()
        {
            var response = await mediator.Send(new GetTeams.Query());
            return response == null ? NotFound() : Ok(response.Teams);
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<TeamDTO>> GetTeamById(int id)
        {
            var response = await mediator.Send(new GetTeamById.Query(id));
            return response == null ? NotFound() : Ok(response.Team);
        }

        [HttpGet("sorted-teams-with-age-restriction")]
        public async Task<ActionResult<IEnumerable<SortedTeamWithAgeRestriction>>> GetSortedTeamsWithAgeRestriction()
        {
            var response = await mediator.Send(new GetSortedMembersOfTeamsOlderThan10Years.Query());
            return response == null ? NotFound() : Ok(response);
        }

        [HttpPost("")]
        public async Task<ActionResult<int>> AddTeam(TeamDTO team)
        {
            var response = await mediator.Send(new AddTeam.Command(team));
            return response == null ? NotFound() : Ok(response);
        }

        [HttpDelete("{teamId:int}")]
        public async Task<ActionResult<TeamDTO>> DeleteTeam(int teamId)
        {
            var response = await mediator.Send(new DeleteTeam.Command(teamId));
            return response == null ? NotFound() : Ok(response.Team);
        }

        [HttpPut("")]
        public async Task<ActionResult<TeamDTO>> UpdateTeam(TeamDTO team)
        {
            var response = await mediator.Send(new UpdateTeam.Command(team));
            return response == null ? NotFound() : Ok(response);
        }
    }
}
