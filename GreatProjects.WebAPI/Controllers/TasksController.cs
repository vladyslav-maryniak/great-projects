﻿using GreatProjects.WebAPI.Commands;
using GreatProjects.WebAPI.DTOs;
using GreatProjects.WebAPI.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GreatProjects.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly IMediator mediator;

        public TasksController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<TaskDTO>>> GetTasks()
        {
            var response = await mediator.Send(new GetTasks.Query());
            return response == null ? NotFound() : Ok(response.Tasks);
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<TaskDTO>> GetTaskById(int id)
        {
            var response = await mediator.Send(new GetTaskById.Query(id));
            return response == null ? NotFound() : Ok(response.Task);
        }

        [HttpPost("")]
        public async Task<ActionResult<int>> AddTask(TaskDTO task)
        {
            var response = await mediator.Send(new AddTask.Command(task));
            return response == null ? NotFound() : Ok(response);
        }

        [HttpDelete("{taskId:int}")]
        public async Task<ActionResult<TaskDTO>> DeleteTask(int taskId)
        {
            var response = await mediator.Send(new DeleteTask.Command(taskId));
            return response == null ? NotFound() : Ok(response.Task);
        }

        [HttpPut("")]
        public async Task<ActionResult<TaskDTO>> UpdateTask(TaskDTO task)
        {
            var response = await mediator.Send(new UpdateTask.Command(task));
            return response == null ? NotFound() : Ok(response);
        }
    }
}
