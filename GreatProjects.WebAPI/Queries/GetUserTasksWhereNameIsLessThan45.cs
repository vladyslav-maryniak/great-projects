﻿using GreatProjects.BLL;
using GreatProjects.DAL.DataAccess;
using GreatProjects.SharedLibrary.ReadModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace GreatProjects.WebAPI.Queries
{
    public class GetUserTasksWhereNameIsLessThan45
    {
        public record Query(int UserId) : IRequest<IAsyncEnumerable<RestrictedTask>>;

        public class Handler : IRequestHandler<Query, IAsyncEnumerable<RestrictedTask>>
        {
            private readonly ProjectContext projectContext;

            public Handler(ProjectContext projectContext)
            {
                this.projectContext = projectContext;
            }

            public Task<IAsyncEnumerable<RestrictedTask>> Handle(Query request, CancellationToken cancellationToken)
            {
                var requests = new SpecialRequests(projectContext);
                var collection = Task.FromResult(requests.GetUserTasksWithRestrictedNameLength(request.UserId, lengthRestriction: 45)
                        .AsAsyncEnumerable());

                return collection;
            }
        }
    }
}
