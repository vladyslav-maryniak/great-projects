﻿using GreatProjects.BLL;
using GreatProjects.DAL.DataAccess;
using GreatProjects.SharedLibrary.ReadModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace GreatProjects.WebAPI.Queries
{
    public class GetSortedUsersWithSortedTasks
    {
        public record Query() : IRequest<IAsyncEnumerable<UserWithSortedTaskNames>>;

        public class Handler : IRequestHandler<Query, IAsyncEnumerable<UserWithSortedTaskNames>>
        {
            private readonly ProjectContext projectContext;

            public Handler(ProjectContext projectContext)
            {
                this.projectContext = projectContext;
            }

            public Task<IAsyncEnumerable<UserWithSortedTaskNames>> Handle(Query request, CancellationToken cancellationToken)
            {
                var requests = new SpecialRequests(projectContext);
                var collection = Task.FromResult(requests.GetSortedUsersWithSortedTasks()
                        .AsAsyncEnumerable());

                return collection;
            }
        }
    }
}
