﻿using AutoMapper;
using GreatProjects.DAL.DataAccess;
using GreatProjects.WebAPI.DTOs;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace GreatProjects.WebAPI.Queries
{
    public class GetTaskById
    {
        public record Query(int Id) : IRequest<Response>;

        public class Handler : IRequestHandler<Query, Response>
        {
            private readonly IMapper mapper;
            private readonly ProjectContext projectContext;

            public Handler(IMapper mapper, ProjectContext projectContext)
            {
                this.mapper = mapper;
                this.projectContext = projectContext;
            }

            public Task<Response> Handle(Query request, CancellationToken cancellationToken)
            {
                var task = projectContext.Tasks.FirstOrDefault(x => x.Id == request.Id);
                return Task.FromResult(task == null ? null : new Response(mapper.Map<TaskDTO>(task)));
            }
        }

        public record Response(TaskDTO Task);
    }
}
