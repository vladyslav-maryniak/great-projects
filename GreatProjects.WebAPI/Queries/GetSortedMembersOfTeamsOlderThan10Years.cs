﻿using GreatProjects.BLL;
using GreatProjects.DAL.DataAccess;
using GreatProjects.SharedLibrary.ReadModels;
using MediatR;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace GreatProjects.WebAPI.Queries
{
    public class GetSortedMembersOfTeamsOlderThan10Years
    {
        public record Query() : IRequest<IEnumerable<SortedTeamWithAgeRestriction>>;

        public class Handler : IRequestHandler<Query, IEnumerable<SortedTeamWithAgeRestriction>>
        {
            private readonly ProjectContext projectContext;

            public Handler(ProjectContext projectContext)
            {
                this.projectContext = projectContext;
            }

            public Task<IEnumerable<SortedTeamWithAgeRestriction>> Handle(Query request, CancellationToken cancellationToken)
            {
                var requests = new SpecialRequests(projectContext);
                var collection = Task.FromResult(requests.GetSortedTeamsWithRestrictedMemberAge(ageRestriction: 10)
                        .AsEnumerable());

                return collection;
            }
        }
    }
}
