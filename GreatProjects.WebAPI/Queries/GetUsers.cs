﻿using AutoMapper;
using GreatProjects.DAL.DataAccess;
using GreatProjects.WebAPI.DTOs;
using MediatR;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace GreatProjects.WebAPI.Queries
{
    public static class GetUsers
    {
        public record Query() : IRequest<Response>;

        public class Handler : IRequestHandler<Query, Response>
        {
            private readonly IMapper mapper;
            private readonly ProjectContext projectContext;

            public Handler(IMapper mapper, ProjectContext projectContext)
            {
                this.mapper = mapper;
                this.projectContext = projectContext;
            }

            public Task<Response> Handle(Query request, CancellationToken cancellationToken)
            {
                var users = projectContext.Users;
                return Task.FromResult(users == null ? null : new Response(users.Select(x => mapper.Map<UserDTO>(x))));
            }
        }

        public record Response(IEnumerable<UserDTO> Users);
    }
}
