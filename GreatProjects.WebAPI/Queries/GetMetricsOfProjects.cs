﻿using GreatProjects.BLL;
using GreatProjects.DAL.DataAccess;
using GreatProjects.SharedLibrary.ReadModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace GreatProjects.WebAPI.Queries
{
    public class GetMetricsOfProjects
    {
        public record Query() : IRequest<IAsyncEnumerable<ProjectMetrics>>;

        public class Handler : IRequestHandler<Query, IAsyncEnumerable<ProjectMetrics>>
        {
            private readonly ProjectContext projectContext;

            public Handler(ProjectContext projectContext)
            {
                this.projectContext = projectContext;
            }

            public Task<IAsyncEnumerable<ProjectMetrics>> Handle(Query request, CancellationToken cancellationToken)
            {
                var requests = new SpecialRequests(projectContext);
                var collection = Task.FromResult(requests.GetMetricsOfProjects()
                        .AsAsyncEnumerable());

                return collection;
            }
        }
    }
}
