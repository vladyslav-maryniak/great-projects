﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using GreatProjects.DAL.DataAccess;
using GreatProjects.WebAPI.DTOs;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace GreatProjects.WebAPI.Queries
{
    public static class GetProjectById
    {
        public record Query(int Id) : IRequest<Response>;

        public class Handler : IRequestHandler<Query, Response>
        {
            private readonly IMapper mapper;
            private readonly ProjectContext projectContext;

            public Handler(IMapper mapper, ProjectContext projectContext)
            {
                this.mapper = mapper;
                this.projectContext = projectContext;
            }

            public async Task<Response> Handle(Query request, CancellationToken cancellationToken)
            {
                var project = await projectContext.Projects
                    .SingleOrDefaultAsync(x => x.Id == request.Id, cancellationToken);

                return new Response(mapper.Map<ProjectDTO>(project));
            }
        }

        public record Response(ProjectDTO Project);
    }
}
