﻿using AutoMapper;
using GreatProjects.DAL.DataAccess;
using GreatProjects.WebAPI.DTOs;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace GreatProjects.WebAPI.Queries
{
    public class GetTeamById
    {
        public record Query(int Id) : IRequest<Response>;

        public class Handler : IRequestHandler<Query, Response>
        {
            private readonly IMapper mapper;
            private readonly ProjectContext projectContext;

            public Handler(IMapper mapper, ProjectContext projectContext)
            {
                this.mapper = mapper;
                this.projectContext = projectContext;
            }

            public Task<Response> Handle(Query request, CancellationToken cancellationToken)
            {
                var team = projectContext.Teams.FirstOrDefault(x => x.Id == request.Id);
                return Task.FromResult(team == null ? null : new Response(mapper.Map<TeamDTO>(team)));
            }
        }

        public record Response(TeamDTO Team);
    }
}
