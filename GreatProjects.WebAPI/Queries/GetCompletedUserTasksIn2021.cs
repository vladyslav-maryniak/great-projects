﻿using GreatProjects.BLL;
using GreatProjects.DAL.DataAccess;
using GreatProjects.SharedLibrary.ReadModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace GreatProjects.WebAPI.Queries
{
    public class GetCompletedUserTasksIn2021
    {
        public record Query(int UserId) : IRequest<IAsyncEnumerable<CompletedTask>>;

        public class Handler : IRequestHandler<Query, IAsyncEnumerable<CompletedTask>>
        {
            private readonly ProjectContext projectContext;

            public Handler(ProjectContext projectContext)
            {
                this.projectContext = projectContext;
            }

            public Task<IAsyncEnumerable<CompletedTask>> Handle(Query request, CancellationToken cancellationToken)
            {
                var requests = new SpecialRequests(projectContext);
                var collection = Task.FromResult(requests.GetCompletedUserTasksForYear(request.UserId, year: 2021)
                        .AsAsyncEnumerable());

                return collection;
            }
        }
    }
}
