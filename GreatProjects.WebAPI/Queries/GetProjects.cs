﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using GreatProjects.DAL.DataAccess;
using GreatProjects.WebAPI.DTOs;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace GreatProjects.WebAPI.Queries
{
    public static class GetProjects
    {
        public record Query() : IRequest<Response>;

        public class Handler : IRequestHandler<Query, Response>
        {
            private readonly IMapper mapper;
            private readonly ProjectContext projectContext;

            public Handler(IMapper mapper, ProjectContext projectContext)
            {
                this.mapper = mapper;
                this.projectContext = projectContext;
            }

            public async Task<Response> Handle(Query request, CancellationToken cancellationToken)
            {
                var projects = await projectContext.Projects
                    .ProjectTo<ProjectDTO>(mapper.ConfigurationProvider)
                    .ToArrayAsync(cancellationToken);
                return new Response(projects);
            }
        }

        public record Response(IEnumerable<ProjectDTO> Projects);
    }
}
