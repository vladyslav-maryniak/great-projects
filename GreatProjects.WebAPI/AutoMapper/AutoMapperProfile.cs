﻿using AutoMapper;
using GreatProjects.SharedLibrary.Models;
using GreatProjects.WebAPI.DTOs;

namespace GreatProjects.WebAPI.AutoMapper
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Project, ProjectDTO>();
            CreateMap<Task, TaskDTO>();
            CreateMap<Team, TeamDTO>();
            CreateMap<User, UserDTO>();

            CreateMap<ProjectDTO, Project>();
            CreateMap<TaskDTO, Task>();
            CreateMap<TeamDTO, Team>();
            CreateMap<UserDTO, User>();
        }
    }
}
