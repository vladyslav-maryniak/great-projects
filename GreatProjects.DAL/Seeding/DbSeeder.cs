﻿using GreatProjects.SharedLibrary.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;

namespace GreatProjects.DAL.Seeding
{
    public static class DbSeeder
    {
        private static readonly Assembly assembly = Assembly.GetAssembly(typeof(DbSeeder));

        private static IEnumerable<Project> projects;
        private static IEnumerable<SharedLibrary.Models.Task> tasks;
        private static IEnumerable<Team> teams;
        private static IEnumerable<User> users;

        public static async Task<IEnumerable<Project>> GetProjects() =>
            projects ??= await GetEntities<Project>("GreatProjects.DAL.Sources.projects.json");

        public static async Task<IEnumerable<SharedLibrary.Models.Task>> GetTasks() =>
            tasks ??= await GetEntities<SharedLibrary.Models.Task>("GreatProjects.DAL.Sources.tasks.json");

        public static async Task<IEnumerable<Team>> GetTeams() =>
            teams ??= await GetEntities<Team>("GreatProjects.DAL.Sources.teams.json");

        public static async Task<IEnumerable<User>> GetUsers() =>
            users ??= await GetEntities<User>("GreatProjects.DAL.Sources.users.json");

        private static async Task<IEnumerable<T>> GetEntities<T>(string fileName)
        {
            var entities = await ReadEmbeddedFile(fileName);
            return JsonConvert.DeserializeObject<IEnumerable<T>>(entities);
        }

        private static async Task<string> ReadEmbeddedFile(string fileName)
        {
            Stream resourseStream = assembly.GetManifestResourceStream(fileName);
            using (var reader = new StreamReader(resourseStream))
            {
                return await reader.ReadToEndAsync();
            }
        }
    }
}
