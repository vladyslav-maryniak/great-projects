﻿using GreatProjects.DAL.Seeding;
using GreatProjects.SharedLibrary.Models;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace GreatProjects.DAL.DataAccess
{
    public class ProjectContext : DbContext
    {
        public ProjectContext(DbContextOptions options) : base(options) { }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<User> Users { get; set; }

        protected override async void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

            modelBuilder.Entity<Project>().HasData(await DbSeeder.GetProjects());
            modelBuilder.Entity<Task>().HasData(await DbSeeder.GetTasks());
            modelBuilder.Entity<Team>().HasData(await DbSeeder .GetTeams());
            modelBuilder.Entity<User>().HasData(await DbSeeder .GetUsers());
        }
    }
}
