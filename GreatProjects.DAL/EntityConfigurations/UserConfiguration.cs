﻿using GreatProjects.SharedLibrary.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GreatProjects.DAL.EntityConfigurations
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.Property(u => u.FirstName)
                .HasMaxLength(20);

            builder.Property(u => u.LastName)
                .HasMaxLength(20);

            builder.Property(u => u.Email)
                .HasMaxLength(50);

            builder.Property(u => u.RegisteredAt)
                .IsRequired();

            builder.Property(u => u.BirthDay)
                .IsRequired();

            builder
                .HasMany(u => u.PerformedTasks)
                .WithOne(t => t.Performer)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
