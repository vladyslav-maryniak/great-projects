﻿using GreatProjects.SharedLibrary.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GreatProjects.DAL.EntityConfigurations
{
    public class TeamConfiguration : IEntityTypeConfiguration<Team>
    {
        public void Configure(EntityTypeBuilder<Team> builder)
        {
            builder.Property(t => t.Name)
                .HasMaxLength(30);

            builder.Property(t => t.CreatedAt)
                .IsRequired();
        }
    }
}
