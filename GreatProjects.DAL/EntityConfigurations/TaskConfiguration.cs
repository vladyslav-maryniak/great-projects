﻿using GreatProjects.SharedLibrary.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GreatProjects.DAL.EntityConfigurations
{
    public class TaskConfiguration : IEntityTypeConfiguration<Task>
    {
        public void Configure(EntityTypeBuilder<Task> builder)
        {
            builder.Property(t => t.ProjectId)
                .IsRequired();

            builder.Property(t => t.PerformerId)
                .IsRequired();

            builder.Property(t => t.Name)
                .HasMaxLength(80);

            builder.Property(t => t.Description)
                .HasMaxLength(100);

            builder.Property(t => t.State)
                .IsRequired();

            builder.Property(t => t.CreatedAt)
                .IsRequired();

            builder
                .HasOne(t => t.Performer)
                .WithMany(u => u.PerformedTasks)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
