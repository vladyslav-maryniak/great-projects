import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormBuilder, NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';

import { Project } from '../project.model';
import { ProjectService } from '../project.service';

@Component({
  selector: 'app-project-detail',
  templateUrl: './project-detail.component.html',
  styleUrls: ['./project-detail.component.css']
})
export class ProjectDetailComponent implements OnInit, OnDestroy {
  project!: Project;
  subscriptions: Subscription[] = [];
  isAddMode: Boolean = false;

  constructor(
    private route: ActivatedRoute,
    private projectService: ProjectService,
    private location: Location,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.getProject();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  onSubmit(form: NgForm) {
    this.isAddMode ? this.addProject() : this.updateProject();
  }

  private getProject(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    if (id === 0) {
      this.isAddMode = true;
      this.project = {} as Project;
      return;
    }

    this.subscriptions.push(
      this.projectService.getProject(id)
        .subscribe(project => this.project = project)
    );
  }

  private addProject(): void {
    if (this.project) {
      this.subscriptions.push(
        this.projectService.addProject(this.project)
          .subscribe()
      );
    }
  }

  private updateProject(): void {
    if (this.project) {
      this.subscriptions.push(
        this.projectService.updateProject(this.project)
          .subscribe()
      );
    }
  }
}
