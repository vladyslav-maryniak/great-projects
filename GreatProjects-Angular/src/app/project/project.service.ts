import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Project } from './project.model';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {
  private projectUrl = 'https://localhost:5001/api/projects';
  private httpOptions = {
    headers: new HttpHeaders({
      'ContentType': 'application/json'
    })
  };

  constructor(
    private http: HttpClient
  ) { }

  getProjects(): Observable<Project[]> {
    return this.http.get<Project[]>(this.projectUrl, this.httpOptions)
      .pipe(
        catchError(this.handleError<Project[]>("getProjects", []))
      );
  }

  getProject(id: number): Observable<Project> {
    return this.http.get<Project>(`${this.projectUrl}/${id}`)
      .pipe(
        catchError(this.handleError<Project>(`getProject id=${id}`))
      );
  }

  updateProject(project: Project): Observable<any> {
    return this.http.put(this.projectUrl, project, this.httpOptions)
      .pipe(
        catchError(this.handleError<Project>("updateProject"))
      );
  }

  addProject(project: Project): Observable<Project> {
    return this.http.post<Project>(this.projectUrl, project, this.httpOptions)
      .pipe(
        catchError(this.handleError<Project>(`addProject`))
      );
  }

  deleteProject(id: number): Observable<Project> {
    return this.http.delete<Project>(`${this.projectUrl}/${id}`, this.httpOptions)
      .pipe(
        catchError(this.handleError<Project>(`deleteProject id=${id}`))
      );
  }

  private handleError<T>(operation = "operation", result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      console.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    }
  }
}
