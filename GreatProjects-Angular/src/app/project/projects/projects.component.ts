import { Component, OnInit } from '@angular/core';

import { Project } from '../project.model';
import { ProjectService } from '../project.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {
  projects: Project[] = [];
  subscriptions: Subscription[] = [];

  constructor(private projectService: ProjectService) { }

  ngOnInit(): void {
    this.getProjects();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  getProjects(): void {
    this.subscriptions.push(
      this.projectService.getProjects()
        .subscribe(projects => this.projects = projects)
    );
  }

  deleteProject(project: Project): void {
    this.projects = this.projects.filter(h => h !== project);
    this.projectService.deleteProject(project.id)
      .subscribe();
  }
}
