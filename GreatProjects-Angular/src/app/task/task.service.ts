import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Task } from './task.model';

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  private taskUrl = 'https://localhost:5001/api/tasks';
  private httpOptions = {
    headers: new HttpHeaders({
      'ContentType': 'application/json'
    })
  };

  constructor(
    private http: HttpClient
  ) { }

  getTasks(): Observable<Task[]> {
    return this.http.get<Task[]>(this.taskUrl, this.httpOptions)
      .pipe(
        catchError(this.handleError<Task[]>("getTasks", []))
      );
  }

  private handleError<T>(operation = "operation", result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    }
  }
}
