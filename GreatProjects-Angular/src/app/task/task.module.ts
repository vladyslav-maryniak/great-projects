import { NgModule } from '@angular/core';

import { TaskRoutingModule } from './task-routing.module';
import { TasksComponent } from './tasks/tasks.component';
import { TaskService } from './task.service';

import { SharedModule } from '../shared/shared.module';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    TasksComponent
  ],
  imports: [
    HttpClientModule,
    TaskRoutingModule,
    SharedModule
  ],
  exports: [
    TasksComponent
  ],
  providers: [
    TaskService
  ]
})
export class TaskModule { }
