import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Team } from './team.model';

@Injectable({
  providedIn: 'root'
})
export class TeamService {
  teamUrl = 'https://localhost:5001/api/teams';
  private httpOptions = {
    headers: new HttpHeaders({
      'ContentType': 'application/json'
    })
  };

  constructor(
    private http: HttpClient
  ) { }

  getTeams(): Observable<Team[]> {
    return this.http.get<Team[]>(this.teamUrl, this.httpOptions)
      .pipe(
        catchError(this.handleError<Team[]>("getTeams", []))
      );
  }

  private handleError<T>(operation = "operation", result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    }
  }
}
