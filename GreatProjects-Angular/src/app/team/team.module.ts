import { NgModule } from '@angular/core';

import { TeamRoutingModule } from './team-routing.module';
import { TeamsComponent } from './teams/teams.component';
import { TeamService } from './team.service';

import { SharedModule } from '../shared/shared.module';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    TeamsComponent
  ],
  imports: [
    HttpClientModule,
    TeamRoutingModule,
    SharedModule
  ],
  exports: [
    TeamsComponent
  ],
  providers: [
    TeamService
  ]
})
export class TeamModule { }
