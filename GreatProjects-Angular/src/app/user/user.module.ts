import { NgModule } from '@angular/core';

import { UserRoutingModule } from './user-routing.module';
import { UsersComponent } from './users/users.component';
import { UserService } from './user.service';

import { SharedModule } from '../shared/shared.module';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    UsersComponent
  ],
  imports: [
    HttpClientModule,
    UserRoutingModule,
    SharedModule
  ],
  exports: [
    UsersComponent
  ],
  providers: [
    UserService
  ]
})
export class UserModule { }
