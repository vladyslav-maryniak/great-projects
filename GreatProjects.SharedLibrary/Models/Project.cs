﻿using System;
using System.Collections.Generic;

namespace GreatProjects.SharedLibrary.Models
{
    public class Project
    {
        public int Id { get; set; }
        public int AuthorId { get; set; }
        public int TeamId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }

        public User Author { get; set; }
        public Team Team { get; set; }
        public List<Task> Tasks { get; set; }
    }
}
