﻿namespace GreatProjects.SharedLibrary.Models
{
    public enum TaskState
    {
        ToDo,
        InProgress,
        Done,
        Canceled
    }
}
