﻿using System;
using System.Collections.Generic;

namespace GreatProjects.SharedLibrary.ReadModels
{
    public class SortedTeamWithAgeRestriction
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<RestrictedUser> Members { get; set; }

        public override bool Equals(object obj)
        {
            return obj is SortedTeamWithAgeRestriction restriction &&
                   Id == restriction.Id &&
                   Name == restriction.Name &&
                   EqualityComparer<IEnumerable<RestrictedUser>>.Default.Equals(Members, restriction.Members);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id, Name, Members);
        }

        public class RestrictedUser
        {
            public int Id { get; set; }
            public DateTime RegisteredAt { get; set; }
            public DateTime BirthDay { get; set; }

            public override bool Equals(object obj)
            {
                return obj is RestrictedUser user &&
                       Id == user.Id &&
                       RegisteredAt == user.RegisteredAt &&
                       BirthDay == user.BirthDay;
            }

            public override int GetHashCode()
            {
                return HashCode.Combine(Id, RegisteredAt, BirthDay);
            }
        }
    }
}
