﻿using System;
using System.Collections.Generic;

namespace GreatProjects.SharedLibrary.ReadModels
{
    public class UserMetrics
    {
        public int Id { get; set; }
        public int LastProjectId { get; set; }
        public int NumberOfTasksOfLastProject { get; set; }
        public int NumberOfUnfinishedOrCancelledTasks { get; set; }
        public LongestTask Task { get; set; }

        public override bool Equals(object obj)
        {
            return obj is UserMetrics metrics &&
                   Id == metrics.Id &&
                   LastProjectId == metrics.LastProjectId &&
                   NumberOfTasksOfLastProject == metrics.NumberOfTasksOfLastProject &&
                   NumberOfUnfinishedOrCancelledTasks == metrics.NumberOfUnfinishedOrCancelledTasks &&
                   EqualityComparer<LongestTask>.Default.Equals(Task, metrics.Task);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id, LastProjectId, NumberOfTasksOfLastProject, NumberOfUnfinishedOrCancelledTasks, Task);
        }

        public class LongestTask
        {
            public int Id { get; set; }
            public DateTime CreatedAt { get; set; }
            public DateTime FinishedAt { get; set; }

            public override bool Equals(object obj)
            {
                return obj is LongestTask task &&
                       Id == task.Id &&
                       CreatedAt == task.CreatedAt &&
                       FinishedAt == task.FinishedAt;
            }

            public override int GetHashCode()
            {
                return HashCode.Combine(Id, CreatedAt, FinishedAt);
            }
        }
    }
}
