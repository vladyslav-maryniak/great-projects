﻿using GreatProjects.SharedLibrary.Models;
using System;
using System.Collections.Generic;

namespace GreatProjects.SharedLibrary.ReadModels
{
    public class ProjectMetrics
    {
        public int Id { get; set; }
        public LongestTask Longest { get; set; }
        public ShortestTask Shortest { get; set; }
        public int NumberOfMembers { get; set; }

        public override bool Equals(object obj)
        {
            return obj is ProjectMetrics metrics &&
                   Id == metrics.Id &&
                   EqualityComparer<LongestTask>.Default.Equals(Longest, metrics.Longest) &&
                   EqualityComparer<ShortestTask>.Default.Equals(Shortest, metrics.Shortest) &&
                   NumberOfMembers == metrics.NumberOfMembers;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id, Longest, Shortest, NumberOfMembers);
        }

        public class LongestTask
        {
            public int? Id { get; set; }
            public string Description { get; set; }
            
            public LongestTask() { }
            
            public LongestTask(Task task)
            {
                Id = task?.Id;
                Description = task?.Description;
            }

            public override bool Equals(object obj)
            {
                return obj is LongestTask task &&
                       Id == task.Id &&
                       Description == task.Description;
            }

            public override int GetHashCode()
            {
                return HashCode.Combine(Id, Description);
            }
        }

        public class ShortestTask
        {
            public int? Id { get; set; }
            public string Name { get; set; }

            public ShortestTask() { }

            public ShortestTask(Task task)
            {
                Id = task?.Id;
                Name = task?.Name;
            }

            public override bool Equals(object obj)
            {
                return obj is ShortestTask task &&
                       Id == task.Id &&
                       Name == task.Name;
            }

            public override int GetHashCode()
            {
                return HashCode.Combine(Id, Name);
            }
        }
    }
}
