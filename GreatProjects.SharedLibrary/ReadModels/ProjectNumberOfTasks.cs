﻿using System;

namespace GreatProjects.SharedLibrary.ReadModels
{
    public class ProjectNumberOfTasks
    {
        public int ProjectId { get; set; }
        public int NumberOfTasks { get; set; }

        public override bool Equals(object obj)
        {
            return obj is ProjectNumberOfTasks tasks &&
                   ProjectId == tasks.ProjectId &&
                   NumberOfTasks == tasks.NumberOfTasks;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(ProjectId, NumberOfTasks);
        }
    }
}
