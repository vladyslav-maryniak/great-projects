﻿using System;

namespace GreatProjects.SharedLibrary.ReadModels
{
    public class CompletedTask
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime FinishedAt { get; set; }

        public override bool Equals(object obj)
        {
            return obj is CompletedTask task &&
                   Id == task.Id &&
                   Name == task.Name &&
                   FinishedAt == task.FinishedAt;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id, Name, FinishedAt);
        }
    }
}
