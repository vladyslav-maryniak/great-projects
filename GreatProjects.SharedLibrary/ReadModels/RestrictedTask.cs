﻿using System;

namespace GreatProjects.SharedLibrary.ReadModels
{
    public class RestrictedTask
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int NameLength { get; set; }

        public override bool Equals(object obj)
        {
            return obj is RestrictedTask task &&
                   Id == task.Id &&
                   Name == task.Name &&
                   NameLength == task.NameLength;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id, Name, NameLength);
        }
    }
}
