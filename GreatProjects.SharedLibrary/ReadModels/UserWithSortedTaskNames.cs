﻿using System;
using System.Collections.Generic;

namespace GreatProjects.SharedLibrary.ReadModels
{
    public class UserWithSortedTaskNames
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public IEnumerable<Task> TaskNames { get; set; }

        public override bool Equals(object obj)
        {
            return obj is UserWithSortedTaskNames names &&
                   Id == names.Id &&
                   FirstName == names.FirstName &&
                   EqualityComparer<IEnumerable<Task>>.Default.Equals(TaskNames, names.TaskNames);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id, FirstName, TaskNames);
        }

        public class Task
        {
            public int Id { get; set; }
            public string Name { get; set; }

            public override bool Equals(object obj)
            {
                return obj is Task task &&
                       Id == task.Id &&
                       Name == task.Name;
            }

            public override int GetHashCode()
            {
                return HashCode.Combine(Id, Name);
            }
        }
    }
}
