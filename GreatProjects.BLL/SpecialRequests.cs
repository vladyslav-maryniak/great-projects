﻿using GreatProjects.DAL.DataAccess;
using GreatProjects.SharedLibrary.ReadModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace GreatProjects.BLL
{
    public class SpecialRequests : ISpecialRequests
    {
        private ProjectContext projectContext;
        
        public SpecialRequests(ProjectContext projectContext)
        {
            this.projectContext = projectContext;
        }

        public IQueryable<ProjectNumberOfTasks> GetNumberOfTasksInUserProject(int userId)
        {
            var projects = projectContext.Projects
                .Include(p => p.Tasks);

            return projects
                .Where(p => p.AuthorId == userId)
                .Select(p => new ProjectNumberOfTasks
                {
                    ProjectId = p.Id,
                    NumberOfTasks = p.Tasks.Count(t => t.ProjectId == p.Id)
                })
                .AsQueryable();
        }

        public IQueryable<RestrictedTask> GetUserTasksWithRestrictedNameLength(int userId, int lengthRestriction)
        {
            var tasks = projectContext.Tasks;
            
            return tasks
                .Where(t => t.PerformerId == userId && t.Name.Length < lengthRestriction)
                .Select(t => new RestrictedTask
                {
                    Id = t.Id,
                    Name = t.Name,
                    NameLength = t.Name.Length
                })
                .AsQueryable();
        }

        public IQueryable<CompletedTask> GetCompletedUserTasksForYear(int userId, int year)
        {
            var tasks = projectContext.Tasks;

            return tasks
                .Where(t => t.PerformerId == userId && t.FinishedAt != null && t.FinishedAt.Value.Year == year)
                .Select(t => new CompletedTask
                {
                    Id = t.Id,
                    Name = t.Name,
                    FinishedAt = t.FinishedAt.Value
                })
                .AsQueryable();
        }

        public IQueryable<SortedTeamWithAgeRestriction> GetSortedTeamsWithRestrictedMemberAge(int ageRestriction)
        {
            var teams = projectContext.Teams;
            var users = projectContext.Users;

            return teams
                .Select(t => new SortedTeamWithAgeRestriction
                {
                    Id = t.Id,
                    Name = t.Name,
                    Members = users
                        .Where(x => x.TeamId == t.Id)
                        .OrderByDescending(x => x.RegisteredAt)
                        .Select(u => new SortedTeamWithAgeRestriction.RestrictedUser
                        {
                            Id = u.Id,
                            RegisteredAt = u.RegisteredAt,
                            BirthDay = u.BirthDay
                        })
                        .AsEnumerable()
                })
                .Where(t => t.Members.All(u => (DateTime.Now.Year - u.BirthDay.Year) > ageRestriction))
                .AsQueryable();
        }

        public IQueryable<UserWithSortedTaskNames> GetSortedUsersWithSortedTasks()
        {
            var tasks = projectContext.Tasks;
            var users = projectContext.Users;

            return users
                .OrderBy(u => u.FirstName)
                .Select(u => new UserWithSortedTaskNames
                {
                    Id = u.Id,
                    FirstName = u.FirstName,
                    TaskNames = tasks
                        .Where(t => t.PerformerId == u.Id)
                        .OrderByDescending(t => t.Name.Length)
                        .Select(ut => new UserWithSortedTaskNames.Task
                        {
                            Id = ut.Id,
                            Name = ut.Name
                        })
                        .AsEnumerable()
                })
                .AsQueryable();
        }

        public UserMetrics GetUserMetrics(int userId)
        {
            var projects = projectContext.Projects;
            var lastProject = projects
                .Where(x => x.AuthorId == userId)
                .OrderBy(x => x.CreatedAt)
                .Last();

            var tasks = projectContext.Tasks;
            var longestTask = tasks
                .Where(x => x.PerformerId == userId)
                .AsEnumerable()
                .OrderByDescending(x => (x.FinishedAt ?? DateTime.Now) - x.CreatedAt)
                .FirstOrDefault();

            var user = projectContext.Users
                .SingleOrDefault(u => u.Id == userId);

            return new UserMetrics
            {
                Id = user.Id,

                LastProjectId = lastProject.Id,

                NumberOfTasksOfLastProject = tasks
                    .Where(x => x.ProjectId == lastProject.Id)
                    .Count(),

                NumberOfUnfinishedOrCancelledTasks = tasks
                    .Where(x => x.PerformerId == userId && x.FinishedAt == null)
                    .Count(),

                Task = new UserMetrics.LongestTask
                {
                    Id = longestTask.Id,
                    CreatedAt = longestTask.CreatedAt,
                    FinishedAt = longestTask.FinishedAt ?? DateTime.Now
                }
            };
        }

        public IQueryable<ProjectMetrics> GetMetricsOfProjects()
        {
            var projects = projectContext.Projects;
            var tasks = projectContext.Tasks;
            var users = projectContext.Users;

            return projects
                .Select(project => new ProjectMetrics
                {
                    Id = project.Id,

                    Longest = new ProjectMetrics.LongestTask(tasks
                        .Where(x => x.ProjectId == project.Id)
                        .OrderBy(x => x.Description.Length)
                        .LastOrDefault()),

                    Shortest = new ProjectMetrics.ShortestTask(tasks
                        .Where(x => x.ProjectId == project.Id)
                        .OrderBy(x => x.Name.Length)
                        .FirstOrDefault()),

                    NumberOfMembers = users
                        .Where(x => project.TeamId == x.TeamId &&
                            (project.Description.Length > 20 || tasks
                                .Where(x => project.Id == x.ProjectId)
                                .Count() < 3))
                        .Count()
                });
        }
    }
}
