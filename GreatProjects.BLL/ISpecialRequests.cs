﻿using GreatProjects.SharedLibrary.ReadModels;
using System.Linq;

namespace GreatProjects.BLL
{
    public interface ISpecialRequests
    {
        IQueryable<ProjectNumberOfTasks> GetNumberOfTasksInUserProject(int userId);
        IQueryable<RestrictedTask> GetUserTasksWithRestrictedNameLength(int userId, int lengthRestriction);
        IQueryable<CompletedTask> GetCompletedUserTasksForYear(int userId, int year);
        IQueryable<SortedTeamWithAgeRestriction> GetSortedTeamsWithRestrictedMemberAge(int ageRestriction);
        IQueryable<UserWithSortedTaskNames> GetSortedUsersWithSortedTasks();
        UserMetrics GetUserMetrics(int userId);
        IQueryable<ProjectMetrics> GetMetricsOfProjects();
    }
}
