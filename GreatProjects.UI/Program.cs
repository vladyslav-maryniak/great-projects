﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Timers;
using GreatProjects.UI.Localizations;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace GreatProjects
{
    class Program
    {
        private static int exceptionCounter = 0;
        private static int maxHttpRequestExceptions = 5;
        private const int delay = 1000;
        private const int interval = 5000;

        private static IRequests requests = new Requests();
        private static Timer timer = new Timer(interval);
        private static Dictionary<int, Func<Task<string>>> methods = 
            new Dictionary<int, Func<Task<string>>>()
            {
                { 1, GetNumberOfTasksInUserProjects },
                { 2, GetUserTasksWhereNameIsLessThan45 },
                { 3, GetCompletedUserTasksIn2021 },
                { 4, GetSortedMembersOfTeamsOlderThan10Years },
                { 5, GetSortedUsersWithSortedTasks },
                { 6, GetUserMetrics },
                { 7, GetMetricsOfProjects }
            };

        private static async Task Main()
        {
            timer.Elapsed += Timer_Elapsed;
            timer.Start();

            Console.WriteLine(Localization.Title);
            Console.WriteLine($"{Localization.ExitMessage} {Localization.Exit}\n");
            Console.WriteLine($"{Localization.Introduction}\n");

            Console.WriteLine($"{FormatEnumerable(methods.Values.Select(x => x.Method.Name))}\n");

            while (true)
            {
                Console.Write($"{Localization.Method}: ");
                var input = Console.ReadLine();

                if (string.IsNullOrEmpty(input))
                {
                    continue;
                }
                if (input == Localization.Exit)
                {
                    requests.Dispose();
                    break;
                }

                int method;
                if (!int.TryParse(input, out method))
                {
                    Console.WriteLine($"{Localization.Ooops} {Localization.InvalidArgumentFormat}\n");
                    continue;
                }
                if (!methods.ContainsKey(method))
                {
                    Console.WriteLine($"{Localization.Ooops} {Localization.NonExistentMethod}\n");
                    continue;
                }

                var result = await methods[method]();
                if (string.IsNullOrEmpty(result))
                {
                    continue;
                }
                Console.WriteLine($"\n{result}\n");
            }
        }

        private static async void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            var result = await Check<HttpRequestException>(() =>
                   requests.MarkRandomTaskWithDelay(delay));
            
            Console.Write($"\n\n{Localization.TaskIsDone}:" +
                $"\n{GetPrettyJsonString(result)}\n\n{Localization.Method}: ");
        }

        private static async Task<string> GetNumberOfTasksInUserProjects()
        {
            var userId = GetInputInteger("User ID");
            var result = await Check<HttpRequestException>(() =>
                requests.GetNumberOfTasksInUserProjects(userId));
            return GetPrettyJsonString(result);
        }

        private static async Task<string> GetUserTasksWhereNameIsLessThan45()
        {
            var userId = GetInputInteger("User ID");
            var result = await Check<HttpRequestException>(() =>
                requests.GetUserTasksWhereNameIsLessThan45(userId));
            return GetPrettyJsonString(result);
        }

        private static async Task<string> GetCompletedUserTasksIn2021()
        {
            var userId = GetInputInteger("User ID");
            var result = await Check<HttpRequestException>(() =>
                requests.GetCompletedUserTasksIn2021(userId));
            return GetPrettyJsonString(result);
        }

        private static async Task<string> GetSortedMembersOfTeamsOlderThan10Years()
        {
            var result = await Check<HttpRequestException>(() => 
                requests.GetSortedMembersOfTeamsOlderThan10Years());
            return GetPrettyJsonString(result);
        }

        private static async Task<string> GetSortedUsersWithSortedTasks()
        {
            var result = await Check<HttpRequestException>(() =>
                requests.GetSortedMembersOfTeamsOlderThan10Years());
            return GetPrettyJsonString(result);
        }

        private static async Task<string> GetUserMetrics()
        {
            var userId = GetInputInteger("User ID");
            var result = await Check<HttpRequestException>(() =>
                requests.GetUserMetrics(userId));
            return GetPrettyJsonString(result);
        }

        private static async Task<string> GetMetricsOfProjects()
        {
            var result = await Check<HttpRequestException>(() =>
                requests.GetMetricsOfProjects());
            return GetPrettyJsonString(result);
        }

        public static async Task<string> Check<T>(Func<Task<string>> request) where T : Exception
        {
            try
            {
                return await request();
            }
            catch (Exception e) when (e is T)
            {
                if (++exceptionCounter > maxHttpRequestExceptions)
                {
                    Environment.Exit(0);
                }
                Console.WriteLine($"{Localization.Ooops} {Localization.HttpRequestException}\n");
                return string.Empty;
            }
        }

        private static string FormatEnumerable(IEnumerable<string> enumerable)
        {
            int i = 1;
            return string.Join(Environment.NewLine, enumerable.Select(x => $"\t{i++} - " + x));
        }

        private static int GetInputInteger(string argumentName)
        {
            while (true)
            {
                Console.Write($"{argumentName}: ");
                if (int.TryParse(Console.ReadLine(), out int integer))
                {
                    return integer;
                }
                Console.WriteLine($"{Localization.Ooops} {Localization.InvalidArgumentFormat}\n");
            }
        }

        private static string GetPrettyJsonString(string raw)
        {
            return string.IsNullOrEmpty(raw) ? string.Empty : JToken.Parse(raw).ToString(Formatting.Indented);
        }
    }
}
