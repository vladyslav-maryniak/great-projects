﻿using GreatProjects.UI.ClientModels;
using System;

namespace GreatProjects.UI
{
    public static class TaskHandler
    {
        public static void MarkTaskAsDone(Task task)
        {
            if (task != null)
            {
                task.FinishedAt = DateTime.Now;
                task.State = TaskState.Done;
            }
        }
    }
}
