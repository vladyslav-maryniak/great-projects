﻿using GreatProjects.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace GreatProjects
{
    public class Requests : IRequests
    {
        private static HttpClient client;

        public Requests()
        {
            client = new HttpClient
            {
                BaseAddress = new Uri("https://localhost:5001/")
            };

            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<string> GetNumberOfTasksInUserProjects(int userId)
        {
            return await client.GetStringAsync($"api/Users/{userId}/task-count-per-project");
        }

        public async Task<string> GetUserTasksWhereNameIsLessThan45(int userId)
        {
            return await client.GetStringAsync($"api/Users/{userId}/restricted-tasks");
        }

        public async Task<string> GetCompletedUserTasksIn2021(int userId)
        {
            return await client.GetStringAsync($"api/Users/{userId}/completed-tasks");
        }

        public async Task<string> GetSortedMembersOfTeamsOlderThan10Years()
        {
            return await client.GetStringAsync($"api/Teams/sorted-teams-with-age-restriction");
        }

        public async Task<string> GetSortedUsersWithSortedTasks()
        {
            return await client.GetStringAsync($"api/Users/sorted-users-with-sorted-tasks");
        }

        public async Task<string> GetUserMetrics(int userId)
        {
            return await client.GetStringAsync($"api/Users/{userId}/metrics");
        }

        public async Task<string> GetMetricsOfProjects()
        {
            return await client.GetStringAsync($"api/Projects/metrics");
        }

        private async Task<IEnumerable<UI.ClientModels.Task>> GetTasksAsync()
        {
            return await client.GetFromJsonAsync<IEnumerable<UI.ClientModels.Task>>($"api/Tasks");
        }

        public async Task<string> MarkRandomTaskWithDelay(int delay)
        {
            await Task.Delay(delay);
            var tasks = await GetTasksAsync();
            
            var rand = new Random();
            var maxId = tasks.Max(x => x.Id);
            var taskId = rand.Next(1, maxId + 1);
            
            var task = tasks.SingleOrDefault(t => t.Id == taskId);
            if (task == null)
            {
                return string.Empty;
            }
            TaskHandler.MarkTaskAsDone(task);
            
            var response = await client.PutAsJsonAsync($"api/Tasks", task);
            return await response.Content.ReadAsStringAsync();
        }

        public void Dispose()
        {
            client.Dispose();
        }
    }
}
