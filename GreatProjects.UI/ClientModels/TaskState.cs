﻿namespace GreatProjects.UI.ClientModels
{
    public enum TaskState
    {
        ToDo,
        InProgress,
        Done,
        Canceled
    }
}
