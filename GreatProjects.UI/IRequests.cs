﻿using System;
using System.Threading.Tasks;

namespace GreatProjects
{
    public interface IRequests : IDisposable
    {
        Task<string> GetCompletedUserTasksIn2021(int userId);
        Task<string> GetMetricsOfProjects();
        Task<string> GetNumberOfTasksInUserProjects(int userId);
        Task<string> GetSortedMembersOfTeamsOlderThan10Years();
        Task<string> GetSortedUsersWithSortedTasks();
        Task<string> GetUserMetrics(int userId);
        Task<string> GetUserTasksWhereNameIsLessThan45(int userId);
        Task<string> MarkRandomTaskWithDelay(int delay);
    }
}